import './App.css';
import Task1 from './containers/Task1/Task1';
import Task2 from './containers/Task2/Task2';

const App = () => {
  return (
    <>
      <Task1 />
      <Task2 />
    </>
  );
}

export default App;
