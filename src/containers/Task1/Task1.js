import React, { Component } from 'react';
import './Task1.css';
import { nanoid } from 'nanoid';
import InputForm from '../../components/Task1/InputForm/InputForm';
import MovieList from '../../components/Task1/MovieList/MovieList';


class Task1 extends Component {

    state = {
        movieList: [
            { id: nanoid(), name: "Avatar" },
            { id: nanoid(), name: "Godfather" },
            { id: nanoid(), name: "Star Wars" }
        ]
    };

    constructor(props){
        super(props)
        console.log("[Task1] constructor", this.state.movieList)
    };

    addMovieHandler = e => {
        e.preventDefault();
        const listCopy = [...this.state.movieList];
        listCopy.push({ id: nanoid(), name: document.getElementById("addMovie").value });
        this.setState({ movieList: listCopy });
    };

    removeMovieHandler = id => {
        const listCopy = [...this.state.movieList];
        const index = listCopy.findIndex(item => item.id === id);
        listCopy.splice(listCopy[index], 1);
        this.setState({ movieList: listCopy });
    };

    changeMovieName = (event, id) => {
        const listCopy = [...this.state.movieList];
        const index = listCopy.findIndex(item => item.id === id);
        const movieCopy = listCopy[index];
        movieCopy.name = event.target.value;
        listCopy[index] = movieCopy;
        this.setState({ movieList: listCopy });
    };

    componentDidMount(){
        console.log('[Task1] did mount');
    };
    render() {
        console.log('[Task1] render');
        return (
            <div className="Task">
                <h3>Task 1</h3>
                <InputForm add={this.addMovieHandler} />
                <h3>To watch list:</h3>
                <div>
                    <MovieList
                        movieList={this.state.movieList}
                        removeMovie={this.removeMovieHandler}
                        changeMovieName={this.changeMovieName}
                    />
                </div>
            </div>
        );
    };
};

export default Task1;