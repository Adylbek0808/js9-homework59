import React, { Component } from 'react';
import ButtonsMenu from '../../components/Task2/ButtonsMenu/ButtonsMenu';
import JokesList from '../../components/Task2/JokesList/JokesList';


const url = "https://api.chucknorris.io/jokes/random";

class Task2 extends Component {

    state = {
        jokes: [],
        jokesNum: 3
    };

    getJoke = () => {
        const data = [...this.state.jokes];
        const fetchData = async () => {
            const response = await fetch(url);
            if (response.ok) {
                const joke = await response.json();
                data.push(joke);
                this.setState({ jokes: data });
            };
        };
        fetchData().catch(console.error);
    };

    getJokes = num => {
        const data = [];
        const jokesList = [];
        for (let i = 0; i < num; i++) {
            data.push(fetch(url));
        };
        Promise.all(
            data
        ).then(items => {
            items.map(async (item) => {
                const joke = await item.json();
                jokesList.push(joke);
                this.setState({ jokes: jokesList });
            });
        }).catch(console.error);
    };

    jokeNumHandler = event => {
        let numCopy = this.state.jokesNum;
        numCopy = event.target.value;
        this.setState({ jokesNum: numCopy });
    }

    componentDidMount() {
        console.log('[Task2] did mount');
        this.getJokes(this.state.jokesNum);
    }

    render() {
        console.log("[Task2] render")
        return (
            <div className="Task">
                <h3>Task 2</h3>
                <div>
                    <JokesList list={this.state.jokes} />
                    <ButtonsMenu
                        getJokes={this.getJokes}
                        getJoke={this.getJoke}
                        jokesNum={this.state.jokesNum}
                        changeJokesNum={this.jokeNumHandler}
                    />
                </div>
            </div >
        );
    };
};

export default Task2;