import React, { Component } from 'react';

class Button extends Component {
    componentDidUpdate(){
        console.log("[Button] did update")
    };
    shouldComponentUpdate(nextProps, nextState) {
        return nextProps.children !== this.props.children;
    };
    render() {
        return (
            <button type="button" onClick={this.props.command}>{this.props.children}</button>
        );
    };
}
;
export default Button;