import React, { Component } from 'react';
import Button from './Button/Button';

class ButtonsMenu extends Component {
    render() {
        return (
            <div>
                <p>
                    <label>How many jokes you need?</label>
                    <input
                        type="number"
                        placeholder="How many jokes you need?"
                        value={this.props.jokesNum}
                        onChange={this.props.changeJokesNum}
                    />
                </p>
                <Button command ={() => this.props.getJokes(this.props.jokesNum)}>Shoe new jokes</Button>
                <Button command ={this.props.getJoke}>Show ONE new joke</Button>
                {/* <button type="button" onClick={() => this.props.getJokes(this.props.jokesNum)}>Show new jokes</button>
                <button type="button" onClick={this.props.getJoke}>Show ONE new Joke</button> */}
            </div>
        );
    };
};

export default ButtonsMenu;