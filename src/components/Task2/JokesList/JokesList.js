import React, { Component } from 'react';
import Joke from '../Joke/Joke';
import { nanoid } from 'nanoid';

class JokesList extends Component {
    render() {
        return this.props.list.map(item => (
            <Joke key={nanoid()} joke={item.value} number={this.props.list.indexOf(item)+1}/>
        ));
    };
};

export default JokesList;