import React, { Component } from 'react';
import './Joke.css';

class Joke extends Component {
    render() {
        return (
            <div className="Joke">
                <h4>Joke {this.props.number}:</h4>
                <p>{this.props.joke}</p>
            </div>
        );
    };
};

export default Joke;