import React, { Component } from 'react';
import './Movie.css';

class Movie extends Component {
    componentDidMount() {
        console.log('[Movie] did mount')
    };
    componentDidUpdate() {
        console.log('[Movie] did update')
    };

    shouldComponentUpdate(nextProps, nextState){
        console.log('[Movie] should update');
        return nextProps.name !== this.props.name
    };
    render() {
        console.log('[Movie] render')
        return (
            <div className="Movie">
                <input className="inField" type="text" value={this.props.name} id={this.props.id} onChange={this.props.changeName} />
                <button type="button" onClick={this.props.remove}>X</button>
            </div >
        );
    };
};

export default Movie;