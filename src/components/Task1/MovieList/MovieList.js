import React, { Component } from 'react';
import Movie from '../Movie/Movie';




class MovieList extends Component {

    componentDidMount() {
        console.log('[MovieList] did mount');
    };
    render() {
        console.log('[MovieList] render')
        return this.props.movieList.map(film => (
            <Movie
                key={film.id}
                name={film.name}
                id={film.id}
                remove={() => this.props.removeMovie(film.id)}
                changeName={event => this.props.changeMovieName(event, film.id)}
            />
        ));
    };
};

export default MovieList;