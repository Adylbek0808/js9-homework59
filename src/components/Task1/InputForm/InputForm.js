import React, { Component } from 'react';

class InputForm extends Component {
    render() {
        return (
            <form className="InputField" onSubmit={this.props.add}>
                <input type="text" placeholder="Enter movie list" id="addMovie" />
                <button type="submit">ADD</button>
            </form>
        );
    }
}

export default InputForm;